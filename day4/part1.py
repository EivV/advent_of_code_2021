"""--- Day 4: Giant Squid ---

You're already almost 1.5km (almost a mile) below the surface of the ocean,
already so deep that you can't see any sunlight. What you can see, however,
is a giant squid that has attached itself to the outside of your submarine.

Maybe it wants to play bingo?

Bingo is played on a set of boards each consisting of a 5x5 grid of numbers.
Numbers are chosen at random, and the chosen number is marked on all boards on
which it appears. (Numbers may not appear on all boards.) If all numbers in any
row or any column of a board are marked, that board wins. (Diagonals don't
count.)

The submarine has a bingo subsystem to help passengers (currently, you and the
giant squid) pass the time. It automatically generates a random order in which
to draw numbers and a random set of boards (your puzzle input). For example:

7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7

After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no
winners, but the boards are marked as follows (shown here adjacent to each
other to save space):

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are
still no winners:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

Finally, 24 is drawn:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

At this point, the third board wins because it has at least one complete row or
column of marked numbers (in this case, the entire top row is marked:
14 21 17 24 4).

The score of the winning board can now be calculated. Start by finding the sum
of all unmarked numbers on that board; in this case, the sum is 188. Then,
multiply that sum by the number that was just called when the board won, 24,
to get the final score, 188 * 24 = 4512.

To guarantee victory against the giant squid, figure out which board will win
first. What will your final score be if you choose that board?

"""
import re
from dataclasses import dataclass, field
from itertools import filterfalse
from operator import attrgetter


@dataclass
class Entry:
    value: int
    marked: bool = False

    def __post_init__(self):
        self.value = int(self.value)

    def __eq__(self, other):
        if not isinstance(other, Entry):
            return False

        return self.value == other.value


@dataclass
class Line:
    entries: list[Entry] = field(default_factory=list)

    def __repr__(self):
        strings = []
        for e in self.entries:
            string = f"{e.value}"
            if e.marked:
                string = f"({string})"
            strings.append(string)
        return ' '.join(strings)

    def __iter__(self):
        return iter(self.entries)

    def __next__(self):
        return next(self)

    def add_entry(self, entry: Entry):
        self.entries.append(entry)

    def is_bingo(self) -> bool:
        for e in self.entries:
            if not e.marked:
                return False

        return True


@dataclass
class Board:
    rows: list[Line] = field(default_factory=list)
    cols: list[Line] = field(default_factory=list)

    def __str__(self):
        return '\n'.join(str(row) for row in self.rows)

    def __repr__(self):
        return str(self)

    def add_line(self, line: Line):
        self.rows.append(line)
        for idx, e in enumerate(line):
            try:
                self.cols[idx]
            except IndexError:
                self.cols.append(Line())

            board_line = self.cols[idx]
            board_line.add_entry(e)

    def mark_entry(self, entry: Entry):
        for row in self.rows:
            for e in row:
                if e == entry:
                    e.marked = True

    def is_bingo(self) -> bool:
        if any(row.is_bingo() for row in self.rows):
            return True
        if any(col.is_bingo() for col in self.cols):
            return True

        return False


@dataclass
class Draws:
    entries: list[Entry]

    def __iter__(self):
        return iter(self.entries)

    def __next__(self):
        return next(self)


def reader(string: str):
    lines = string.splitlines()
    it = iter(lines)
    first_line = next(it)
    draws = Draws(list(map(Entry, first_line.split(','))))

    board = None
    boards = []
    for line in it:
        if not line:
            if board is not None:  # First board is not created yet.
                boards.append(board)
            board = Board()
        else:
            board.add_line(
                Line(list(
                    map(
                        Entry,
                        re.findall(r'\d+', line)
                    )
                ))
            )

    return draws, boards


class Bingo(Exception):
    ...


def solution(draws: Draws, boards: list[Board]) -> int:
    winning_board = None
    winning_draw = None
    try:
        for draw in draws:
            # print(f"{draw=}")
            for board in boards:
                board.mark_entry(draw)
                if board.is_bingo():
                    winning_board = board
                    winning_draw = draw
                    raise Bingo()
    except Bingo:
        ...

    # print(f"{winning_board=}\n\n")
    # print(f"{winning_draw=}\n\n")
    # for board in boards:
    #     print(board, '\n')

    sums = 0
    for row in winning_board.rows:
        sums += sum(map(attrgetter('value'), filterfalse(attrgetter('marked'), row)))

    # print(f"{sums=}")

    return sums * winning_draw.value


def test_solution():
    with open("in.txt", "r") as f:
        string = f.read()
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 49686


def test_example():
    string = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7

"""
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 4512


def test_simple1():
    string = """1,7,4,9,5

7 8
4 9

1 5
9 4

"""
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 68
