"""--- Part Two ---

On the other hand, it might be wise to try a different strategy: let the giant
squid win.

You aren't sure how many bingo boards a giant squid could play at once, so
rather than waste time counting its arms, the safe thing to do is to figure
out which board will win last and choose that one. That way, no matter which
boards it picks, it will win for sure.

In the above example, the second board is the last to win, which happens after
13 is eventually called and its middle column is completely marked. If you were
to keep playing until this point, the second board would have a sum of unmarked
numbers equal to 148 for a final score of 148 * 13 = 1924.

Figure out which board will win last. Once it wins, what would its final score
be?

"""
import re
from dataclasses import dataclass, field
from itertools import filterfalse
from operator import attrgetter


@dataclass
class Entry:
    value: int
    marked: bool = False

    def __post_init__(self):
        self.value = int(self.value)

    def __eq__(self, other):
        if not isinstance(other, Entry):
            return False

        return self.value == other.value


@dataclass
class Line:
    entries: list[Entry] = field(default_factory=list)

    def __repr__(self):
        strings = []
        for e in self.entries:
            string = f"{e.value}"
            if e.marked:
                string = f"({string})"
            strings.append(string)
        return ' '.join(strings)

    def __iter__(self):
        return iter(self.entries)

    def __next__(self):
        return next(self)

    def add_entry(self, entry: Entry):
        self.entries.append(entry)

    def is_bingo(self) -> bool:
        for e in self.entries:
            if not e.marked:
                return False

        return True


@dataclass
class Board:
    rows: list[Line] = field(default_factory=list)
    cols: list[Line] = field(default_factory=list)
    won: bool = False

    def __str__(self):
        return '\n'.join(str(row) for row in self.rows)

    def __repr__(self):
        return str(self)

    def add_line(self, line: Line):
        self.rows.append(line)
        for idx, e in enumerate(line):
            try:
                self.cols[idx]
            except IndexError:
                self.cols.append(Line())

            board_line = self.cols[idx]
            board_line.add_entry(e)

    def mark_entry(self, entry: Entry):
        for row in self.rows:
            for e in row:
                if e == entry:
                    e.marked = True

    def mark_won(self):
        self.won = True

    def is_bingo(self) -> bool:
        if any(row.is_bingo() for row in self.rows):
            return True
        if any(col.is_bingo() for col in self.cols):
            return True

        return False


@dataclass
class Draws:
    entries: list[Entry]

    def __iter__(self):
        return iter(self.entries)

    def __next__(self):
        return next(self)


def reader(string: str):
    lines = string.splitlines()
    it = iter(lines)
    first_line = next(it)
    draws = Draws(list(map(Entry, first_line.split(','))))

    board = None
    boards = []
    for line in it:
        if not line:
            if board is not None:  # First board is not created yet.
                boards.append(board)
            board = Board()
        else:
            board.add_line(
                Line(list(
                    map(
                        Entry,
                        re.findall(r'\d+', line)
                    )
                ))
            )

    return draws, boards


class Bingo(Exception):
    ...


def solution(draws: Draws, boards: list[Board]) -> int:
    winning_board = None
    winning_draw = None
    for draw in draws:
        for board in filterfalse(attrgetter('won'), boards):
            board.mark_entry(draw)
            if board.is_bingo():
                winning_board = board
                winning_draw = draw
                board.mark_won()

    sums = 0
    for row in winning_board.rows:
        sums += sum(map(attrgetter('value'), filterfalse(attrgetter('marked'), row)))

    return sums * winning_draw.value


def test_solution():
    with open("in.txt", "r") as f:
        string = f.read()
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 26878


def test_example():
    string = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7

"""
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 1924


def test_simple1():
    string = """1,7,4,9,5

7 8
4 9

1 5
9 4

"""
    draws, boards = reader(string)
    result = solution(draws, boards)
    print(f"\n\n{result}")
    assert result == 68
