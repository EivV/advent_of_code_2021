from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum
from operator import attrgetter
from typing import Generator, Optional


class D(Enum):
    nw = "↖"
    n = "↑"
    ne = "↗"
    e = "→"
    se = "↘"
    s = "↓"
    sw = "↙"
    w = "←"


OPPS = {
    D.nw: D.se,
    D.n: D.s,
    D.ne: D.sw,
    D.e: D.w,
    D.se: D.nw,
    D.s: D.n,
    D.sw: D.ne,
    D.w: D.e,
}
VECTORS = {
    # x, y
    D.nw: (-1, -1),
    D.n: (0, -1),
    D.ne: (1, -1),
    D.e: (1, 0),
    D.se: (1, 1),
    D.s: (0, 1),
    D.sw: (-1, 1),
    D.w: (-1, 0),
}


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __repr__(self):
        return f"┼({self.x},{self.y})"

    @property
    def friends(self) -> Generator[(Point, D), None, None]:
        for direction, (x, y) in VECTORS.items():
            yield Point(self.x + x, self.y + y), direction


@dataclass
class Pus:
    val: Optional[int] = None
    dirs: dict[D, Pus] = field(default_factory=dict)

    def __post_init__(self):
        if isinstance(self.val, str):
            self.val = int(self.val)

    def __repr__(self):
        friends = {d.value: p.val for d, p in self.dirs.items()}
        return f"{self.val}: {friends}"

    def link(self, pus: Pus, from_where: D):
        self.dirs[OPPS[from_where]] = pus

    def tick(self):
        if self.val is None:
            return  # Energy expended.

        self.val += 1
        if self.val >= 10:
            self.val = None
            for pus in self.dirs.values():
                pus.tick()

    def recharge(self):
        if self.val is None:
            self.val = 0
            return 1

        return 0


@dataclass
class Board:
    grid: dict[Point, Pus] = field(default_factory=dict)
    flashes: int = 0
    instant_flashes: int = 0

    def __str__(self):
        y = -1
        string = "Board["
        for p in sorted(self.grid.keys(), key=attrgetter("y", "x")):
            if p.y > y:
                y = p.y
                string += "\n"
            string += f"{self.grid[p].val:1}"
        string += "\n]"
        return string

    def add_pus(self, pus: Pus, where: Point):
        self.grid[where] = pus
        for friend_point, direction in where.friends:
            if friend_point in self.grid:
                self.grid[friend_point].link(pus, direction)
                pus.link(self.grid[friend_point], OPPS[direction])

    def tick(self):
        self.instant_flashes = 0
        for pus in self.grid.values():
            pus.tick()
        for pus in self.grid.values():
            recharged = pus.recharge()
            self.flashes += recharged
            self.instant_flashes += recharged


def reader(string: str) -> Board:
    lines = string.splitlines()
    b = Board()

    for y, line in enumerate(lines):
        for x, val in enumerate(line):
            pus = Pus(val)
            b.add_pus(pus, Point(x, y))

    return b
