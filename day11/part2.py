"""--- Part Two ---

It seems like the individual flashes aren't bright enough to navigate. However,
you might have a better option: the flashes seem to be synchronizing!

In the example above, the first time all octopuses flash simultaneously is step
195:

After step 193:
5877777777
8877777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777

After step 194:
6988888888
9988888888
8888888888
8888888888
8888888888
8888888888
8888888888
8888888888
8888888888
8888888888

After step 195:
0000000000
0000000000
0000000000
0000000000
0000000000
0000000000
0000000000
0000000000
0000000000
0000000000

If you can calculate the exact moments when the octopuses will all flash
simultaneously, you should be able to navigate through the cavern. What is the
first step during which all octopuses flash?

"""
from __future__ import annotations

from core import Board, reader


def solution(board: Board) -> int:
    print("beginning board:", board)

    counter = 0
    while board.instant_flashes != len(board.grid):
        counter += 1
        board.tick()

    return counter


def test_solution():
    with open("in.txt", "r") as f:
        string = f.read()
    board = reader(string)
    result = solution(board)
    print(f"\n\n{result}")
    assert result == 327


def test_example1():
    string = """5877777777
8877777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777
7777777777
"""
    board = reader(string)
    result = solution(board)
    print(f"\n\n{result}")
    assert result == 2
